$(document).ready(function() {

  // external links in new window
    $("a[href^='http://']").attr("target","_blank");


    // open langmenu's dropdown list
    $('p.selected').addClass('original').clone().removeClass('original').prependTo('.langmenu');
    $('p.selected.original').remove();
    $('.langmenu').click(function() {
      $(this).toggleClass('open');
    });
    // events on vertical scroll
    $(window).scroll(function() {
        $('.langmenu').removeClass('open');
    });

});
